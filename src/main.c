#define _GNU_SOURCE

#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <errno.h>
#include <argp.h>
#include <err.h>
#include "../lib/libbmr/bmr.h"
#include "../lib/time-string/time-string.h"

struct args {
	double height; // meters
	double age;
	double weight; // kg
	enum bmr_sex sex;
} static args = {
	.height = -1,
	.age = -1,
	.weight = -1,
	.sex = -1,
};
struct ts_time now;

#define $get_double_oe(_res, _str) \
	if ($bmr_get_double (_res, _str, 0)) \
		errx (1, "%s is not a double", _str);

static error_t parse_opt (int key, char *arg, struct argp_state *state) {

	switch (key) {
			break;
		case 'm':
			$get_double_oe (&args.height, arg);
			break;
		case 'h': {
			if (bmr_parse_height (&args.height, arg, 0))
				errx (1, "\
\"%s\" is a bad hright. Valid is, eg 5ft11 or a float like 1.80, which \
is assumed to be meters", arg);
			break;
		}
		case 'w': {
			if (bmr_parse_weight (&args.weight, arg, 0))
				errx (1, "\
\"%s\" is a bad weight. Valid is, eg 13st2 or a float like 80.3, which \
is assumed to be kg", arg);
			break;
		}
		case 'a':
			$get_double_oe (&args.age, arg);
			break;
		case 'b': {
			struct ts_time birth = ts_time_from_str (arg, 0, &now, 0);
			if (ts_errno)
				errx (1, "\
Couldn't get date-of-birth from %s (format: yyyy-mm-ddTHH:MM:SS)", arg);
			args.age = TS_YEARS_FROM_SECS (now.ts - birth.ts);
		}
			break;
		case 'S':
			if (*arg == 'f')
				args.sex = BMR_SEX_F;
			else if (*arg == 'm')
				args.sex = BMR_SEX_M;
			else
				errx (1, "%s is not a sex. Valid is \"f\" or \"m\"", arg);
			break;
		case ARGP_KEY_INIT:
			break;
		case ARGP_KEY_ARG:
			// Just ignore args. There are no positional params.
			break;
		case ARGP_KEY_END:
			if (args.sex == -1 || args.age == -1 || args.height == -1
					|| args.weight == -1) {
				warnx ("Missing require parameters:");
				if (args.sex == -1)
					warnx ("\t--sex (m/f)");
				if (args.age == -1)
					warnx ("\t--age (a float)");
				if (args.height == -1)
					warnx ("\t--height (eg, 5ft11 or float like 180.0 (meters))");
				if (args.weight == -1)
					warnx ("\t--weight (eg, 13st2 or float like 80.3 (kg))");
				exit (1);
			}
			break;
		default:
			break;
	}
	return 0;
}


int main (int argc, char **argv) {
	now = ts_time_from_ts (time (0));
	struct argp argp = {
		.options = (struct argp_option[]) {
			{"weight", 'w', "FMT", 0,
				"Your weight. A bare float is kg; \"14st2lb\" is stone/pounds \
(you can leave off the \"lb\")"},
			{"height", 'h', "FMT", 0,
				"Your height. A bare float is kg; \"5tf11in\" is height/inches \
(you can leave off the \"in\")"},
			{"age", 'a', "DOUBLE", 0,
				"Your age"},
			{"birth-date", 'b', "TIME STRING", 0,
				"Your date of birth. Can be used instead of age"},
			{"sex", 'S', "STRING", 0,
				"Your sex (m/f)"},
			{0},
		},
		parse_opt,
		"", "\
Calculate your BMR.\v\
",
	};
	argp_parse (&argp, argc, argv, 0, 0, NULL);

	printf ("%.2f\n", bmr_calc (args.weight, args.height, args.age, args.sex));

	exit (0);
}



