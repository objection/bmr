# BMR

This calculates your BMR. You give it your weight, height, sex and
age. That's why it has too many required args, but there's no way
around it. See help for args.

# Install

You need meson and libbmr. meson will be in your repositories (`sudo
dnf install meson`). For libbmr:

```sh

# cd wherever you put your source code
git clone https://gitlab.com/objection/libbmr.git
cd libbmr
meson build
sudo ninja -Cbuild install

```

And to do install bmr itself you do the same thing except with bmr,
not libbmr.

